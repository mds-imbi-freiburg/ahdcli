from averbis import Client, Pear
import os
from pprint import pprint as print
import fire


class AHDcli:
    """ Simple CLI tool to manage PEAR packages on the Averbis Health Discovery Platform."""
    def __init__(self, url=None, token=None, project_name=None):
        """
        Args:
          url: Full url to API endpoint (env variable: AHD_API_URL)
          token: Authentication Token for API access (env variable: AHD_API_TOKEN)
          project_name: Project to manage (env variable: AHD_PROJECT)
        """
        self.url: str = url or os.getenv("AHD_API_URL", "http://localhost:9999/health-discovery")
        self.token = token or os.getenv("AHD_API_TOKEN", None)
        self.project_name = project_name or os.getenv("AHD_PROJECT", "test")
        self.client = Client(self.url, api_token=self.token)
        self.project = self.client.get_project(self.project_name)

    def replace_pear(self, pipeline, old, new):
        self.del_from_pipeline(pipeline, old)
        self.add_to_pipeline(pipeline, new)

    def del_from_pipeline(self, pipeline, pear):
        pipeline = self.project.get_pipeline(pipeline)
        config = pipeline.get_configuration()
        for a in config["annotators"]:
            if a["identifier"].startswith(pear):
                config["annotators"].remove(a)
        pipeline.set_configuration(config)
        print(pipeline.get_configuration())

    def add_to_pipeline(self, pipeline, pear):
        pipeline = self.project.get_pipeline(pipeline)
        pear = Pear(self.project, pear)
        config = pipeline.get_configuration()
        config["annotators"].append(pear.get_default_configuration())
        pipeline.set_configuration(config)
        print(pipeline.get_configuration())


def main():
    fire.Fire(AHDcli)


if __name__ == '__main__':
    fire.Fire(AHDcli)
