#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import find_packages, setup


def load_requirements(fname):
    try:
        from pip._internal.req import parse_requirements
    except ImportError:
        from pip.req import parse_requirements
    reqs = parse_requirements(fname, session="test")
    try:
        return [str(ir.req) for ir in reqs]
    except:
        return [str(ir.requirement) for ir in reqs]


with open("README.md") as f:
    readme = f.read()

setup(
    name="ahdcli",
    description="Simple CLI to the AHD Platform",
    long_description=readme,
    long_description_content_type="text/markdown",
    version="0.0.1",
    author="Fabi T.",
    author_email="fabian.thomczyk@uniklinik-freiburg.de",
    url="https://gitlab.com/mds-imbi-freiburg/ahdcli",
    entry_points={
        "console_scripts": [
            "ahdcli = ahdcli:main",
        ]
    },
    zip_safe=False,
    install_requires=load_requirements("requirements.txt"),
    python_requires=">=3.6",
    license="License :: MIT License",
)
