# Averbis Health Discovery CLI

Simple cli script to manage PEAR packages and other things on the Averbis Health Discovery Platform

## Install
~~~
pip install git+https://gitlab.com/mds-imbi-freiburg/ahdcli.git
~~~

## Usage
By accessing the client and project through the magical fire cli,
one can use almost any method on both of these objects:

### Handling Pipelines
~~~bash
$ ahdcli project get_pipeline discharge get_info
id:                   79
identifier:           l4y_9AnUyovP4LcLWs1F3tgIXaY=
name:                 discharge
description:          no description
pipelineState:        STARTED
pipelineStateMessage: null
preconfigured:        true
~~~

~~~bash
ahdcli project get_pipeline discharge [start|stop]
~~~

### Analysing Text
~~~bash
ahdcli --project_name=Test project get_pipeline discharge analyse_text --annotation_types="*" "Kidney Stone"
~~~

### Manage PEAR's
__Note:__ The API endpoints for managing PEAR's are available from AHD Version 6.

#### List
~~~bash
ahdcli project list_pears
~~~

#### Delete
~~~bash
ahdcli project delete_pear kidney-stone-annotator-0.0.123
~~~

#### Install PEAR
~~~bash
ahdcli project install_pear /tmp/kidney-stone-annotator-0.0.123.pear
~~~

#### Add PEAR to pipeline
~~~bash
ahdcli add_to_pipeline discharge kidney-stone-annotator-0.0.123
~~~

#### Delete from Pipeline
~~~bash
ahdcli del_from_pipeline discharge kidney-stone-annotator-0.0.123
~~~

### Getting Help
For the basic commands see
~~~bash
$ ahdcli - --help

NAME
    ahdcli - Simple CLI tool to manage PEAR packages on the Averbis Health Discovery Platform.
SYNOPSIS
    ahdcli - GROUP | COMMAND | VALUE
DESCRIPTION
    Simple CLI tool to manage PEAR packages on the Averbis Health Discovery Platform.
GROUPS
    GROUP is one of the following:
     client
     project
COMMANDS
    COMMAND is one of the following:
     add_to_pipeline
     del_from_pipeline
     replace_pear
VALUES
    VALUE is one of the following:
     project_name
       Project to manage (env variable: AHD_PROJECT)
     token
       Authentication Token for API access (env variable: AHD_API_TOKEN)
     url
       Full url to API endpoint (env variable: AHD_API_URL)
~~~

From there use any group or command with `ahdcli [COMMAND|GROUP] - --help` and advance deeper into the tree of possibilities.   
For example:

~~~bash
$ ahdcli project get_document_collection COLLECTION import_documents - --help

NAME
    ahdcli project get_document_collection TEST import_documents - Imports documents from a given file. Supported file content types are plain text (text/plain), 

Averbis Solr XML (application/vnd.averbis.solr+xml) and UIMA CAS XMI (application/vnd.uima.cas+xmi).

SYNOPSIS
    ahdcli project get_document_collection TEST import_documents SOURCE <flags>

DESCRIPTION
    [...]

POSITIONAL ARGUMENTS
    SOURCE
        Type:...

FLAGS
    --mime_type=MIME_TYPE
        Type: Optional[str]
        Default: None
    --filename=FILENAME
        Type: Optional[str]
        Default: None
    --typesystem=TYPESYSTEM
        Type: Optional['TypeSystem']
        Default: None

NOTES
    You can also use flags syntax for POSITIONAL ARGUMENTS
~~~
